module Main where

import qualified Data.Map as M

mapS = M.singleton "hello" 3
M.fromList [("hello",1),("bye",2),("hello",3)]

main :: IO ()
main = do
    putStrLn "A"

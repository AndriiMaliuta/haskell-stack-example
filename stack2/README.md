# stack2

```
stack init
stack setup
stack build
```

```haskell
module Main where
import Network.HTTP

import Lib

get :: String -> IO String
get url = simpleHTTP (getRequest url) >>= getResponseBody

-- 2. Get the response code
getCode :: String -> IO ResponseCode
getCode url = simpleHTTP req >>= getResponseCode
    where req = getRequest url

main :: IO ()
main = do
    x <- get "http://google.com"
    putStrLn x
```
